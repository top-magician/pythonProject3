import base64
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ip_port = ('127.0.0.1', 9999)

s = socket.socket()  # 创建套接字
s.connect(ip_port)  # 连接服务器
while True:  # 通过一个死循环不断接收用户输入，并发送给服务器
    byte_list = input("请输入要发送的信息： ")
    if not byte_list:  # 防止输入空信息，导致异常退出
        continue
    file1 = open("hi,python.txt", "a+")
    file1.write(byte_list)
    print('已经写入文件')
    byte_list = byte_list.encode('utf-8')
    encode_byte_list = base64.b32encode(byte_list)
    s.sendall(encode_byte_list)
    if byte_list == "exit":  # 如果输入的是‘exit’，表示断开连接
        print("结束通信！")
        break
    server_reply = s.recv(1024).decode()
    print(server_reply)

s.close()  # 关闭连接
